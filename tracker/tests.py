from django.test import TestCase
from django.urls import reverse
from tracker.models import Story
from django.test import LiveServerTestCase
from selenium.webdriver.firefox.webdriver import WebDriver
import time
from selenium.webdriver.support.wait import WebDriverWait

def create_story(story_name, estimated_time):
    """Create a story with the given `story_name`."""
    return Story.objects.create(story_name=story_name, estimated_time=estimated_time)

class StoryIndexViewTests(TestCase):
    def test_no_stories(self):
        """
        If no stories exist, an appropriate message is displayed.
        """
        response = self.client.get(reverse('tracker:index'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "There are no stories recorded")
        self.assertQuerysetEqual(response.context['stories_list'], [])


    def test_story_representation(self):
        """
        Checking if story is represented in right way
        """
        create_story(story_name="Story XX", estimated_time=25)
        response = self.client.get(reverse('tracker:index'))
        self.assertQuerysetEqual(
            response.context['stories_list'],
            ['<Story: Story XX>']
        )

class MySeleniumTests(LiveServerTestCase):

    @classmethod
    def setUpClass(cls):
        path = 'C:\\Users\\smikis\\Envs\\testproject\\scripts\\extreme\\geckodriver.exe'
        super().setUpClass()
        cls.selenium = WebDriver(executable_path=path)
        cls.selenium.implicitly_wait(10)

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()

    def test_login(self):
        self.selenium.get('%s%s' % (self.live_server_url, '/admin/'))
        self.selenium.get('http://127.0.0.1:8000/admin/')
        username_input = self.selenium.find_element_by_name("username")
        username_input.send_keys('admin')
        password_input = self.selenium.find_element_by_name("password")
        password_input.send_keys('admin')
        self.selenium.find_element_by_xpath('//input[@value="Log in"]').click()