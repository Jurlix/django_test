from django.urls import path
from . import views

app_name = 'tracker'

urlpatterns = [
    path('', views.index, name='index'),
    path('<int:story_id>/', views.tasks, name='tasks'),
    path('<int:story_id>/devs', views.devs, name='devs'),
    path('<int:story_id>/<int:task_id>/', views.times, name='times'),
    ]