from django.db import models

# Create your models here.

class Story(models.Model):
    story_name = models.CharField(max_length=20)
    story_text = models.TextField(max_length=1000)
    estimated_time = models.DurationField(default=0)
    real_time = models.DurationField(default=0, null=True)

    def __str__(self):
        return self.story_name

class Devs(models.Model):
    story = models.ForeignKey(Story, on_delete=models.CASCADE)
    dev_name = models.CharField(max_length=20)
    estimated_time = models.DurationField(default=0)
    real_time = models.DurationField(default=0, null=True)

    def __str__(self):
        return self.dev_name

class Tasks(models.Model):
    story = models.ForeignKey(Story, on_delete=models.CASCADE)
    task_name = models.CharField(max_length=100)
    iteration = models.CharField(max_length=100)
    estimated_task_time = models.DurationField(default=0)
    real_task_time = models.DurationField(default=0, null=True)

    def __str__(self):
        return self.task_name

class Time(models.Model):
    #gal cia nereikia tu foreign key, bent prie story ir dev
    story = models.ForeignKey(Story, on_delete=models.CASCADE)
    task = models.ForeignKey(Tasks, on_delete=models.CASCADE)
    dev = models.ForeignKey(Devs, on_delete=models.CASCADE)
    duration = models.DurationField(default=0, null=True)

    def __str__(self):
        return self.duration
