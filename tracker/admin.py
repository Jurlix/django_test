from django.contrib import admin
from .models import Story, Tasks, Time, Devs

# Register your models here.

admin.site.register(Story)
admin.site.register(Tasks)
admin.site.register(Time)
admin.site.register(Devs)