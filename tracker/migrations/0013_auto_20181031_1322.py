# Generated by Django 2.1.2 on 2018-10-31 11:22

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tracker', '0012_auto_20181031_1230'),
    ]

    operations = [
        migrations.DeleteModel(
            name='Dropdown',
        ),
        migrations.DeleteModel(
            name='Student',
        ),
    ]
