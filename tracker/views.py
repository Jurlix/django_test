from django.shortcuts import render
from django.http import HttpResponse
from django.template import loader
from .models import Story, Tasks, Devs
from django.db.models import Sum
from datetime import timedelta


def index(request):
    # Updating Tasks and Story DB with new elapsed timed as stories page is loaded
    all_tasks = Tasks.objects.all().values_list('id', flat=True)
    for task_id in all_tasks:
        task = Tasks.objects.get(pk=task_id)
        task.real_task_time = task.time_set.aggregate(Sum('duration'))['duration__sum']
        task.save()

    for story_id in Story.objects.all().values_list('id',flat=True):
        story = Story.objects.get(pk=story_id)
        story.real_time = story.tasks_set.aggregate(Sum('real_task_time'))['real_task_time__sum']
        story.save()

    stories_list = Story.objects.all()
    context = {'stories_list': stories_list}
    template = loader.get_template('tracker/index.html')
    return HttpResponse(template.render(context,request))


def devs(request,story_id):
    # Updating Devs DB with new elapsed timed as Developers page is loaded
    all_devs = Devs.objects.all().values_list('id', flat=True)
    for dev_id in all_devs:
        dev = Devs.objects.get(pk=dev_id)
        dev.real_time = dev.time_set.aggregate(Sum('duration'))['duration__sum']
        dev.save()

    dev_info = False
    story_name = Story.objects.get(pk=story_id).story_name

    # Checking if method is POST and adding info for response
    if request.method=="POST":
        dev_info = Devs.objects.get(pk=request.POST['developer'])

    devs_list = Story.objects.get(pk=story_id)
    context = {'devs_list':devs_list, 'story_id':story_id, 'dev_info':dev_info, 'story_name':story_name}
    return render(request, 'tracker/devs.html', context)


def tasks(request, story_id):
    tasks_list = Story.objects.get(pk=story_id)
    context={'tasks_list':tasks_list, 'story_id':story_id}
    return render(request, 'tracker/tasks.html', context)


def times(request, story_id, task_id):
    # Checking request method. If method is POST checking entered times and returning to page with
    # error message if time entered in wrong format
    if request.method == 'POST':
        hours = request.POST['hours']
        minutes = request.POST['minutes']
        try:
            hours = int(hours)
            minutes = int(minutes)
            if hours < 0 or minutes < 0 or minutes > 59:
                raise Exception
        except:
            story = Story.objects.get(pk=story_id)
            task_info = story.tasks_set.get(pk=task_id)
            context = {'task_info': task_info, 'story_id': story_id, 'task_id': task_id, 'story': story, 'error_message': "Time entered in bad format!"}
            return render(request, 'tracker/times.html', context)

    # saving entered time to databases
        dev_id = request.POST['developer']

        task = Tasks.objects.get(pk=task_id)
        task.time_set.create(duration=timedelta(hours=hours, minutes=minutes), story_id=story_id, dev_id=dev_id)
        task.real_task_time = task.time_set.aggregate(Sum('duration'))['duration__sum']
        task.save()

        story = Story.objects.get(pk=story_id)
        story.real_time = story.tasks_set.aggregate(Sum('real_task_time'))['real_task_time__sum']
        story.save()

        dev = story.devs_set.get(pk=dev_id)
        dev.real_time = story.time_set.filter(dev_id = dev_id).aggregate(Sum('duration'))['duration__sum']
        dev.save()

    story = Story.objects.get(pk=story_id)
    task_info = story.tasks_set.get(pk=task_id)
    context={'task_info':task_info, 'story_id':story_id, 'task_id':task_id, 'story':story}
    return render(request, 'tracker/times.html', context)